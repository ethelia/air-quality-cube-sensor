# Air Quality Cube Sensor

Air Quality Cube Sensor Seeduino XIAO firmware. Check [Ethelia project](https://ethelia.pheix.org) for details.

## Credits

1. Sim: https://wokwi.com/projects/398681374455239681
2. QRcode C lib: https://github.com/ricmoo/qrcode/
3. Case from MagiCube: https://www.thingiverse.com/thing:2839525

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
