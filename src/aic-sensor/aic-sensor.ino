#include <U8g2lib.h>
#include <Wire.h>
#include <qrcode.h>  //get library here -> https://www.arduinolibraries.info/libraries/qr-code

/*
 * https://wokwi.com/projects/375224934099639297
 * https://github.com/olikraus/u8g2/wiki/u8g2reference#carduino-example
 *
 */

//U8G2_SSD1306_128X64_NONAME_F_HW_I2C disp(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
U8G2_SH1106_128X64_NONAME_F_HW_I2C disp(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);

#define OLED_WIDTH 64
#define OLED_HEIGHT 64
#define STRLEN 6

uint16_t writecount = 0;

void loop() {
    char header[STRLEN];
    char message[STRLEN];

    writecount++;

    sprintf(message, "%03d", random(17, 30));
    sprintf(header, "%s", "PM2.5");

    disp.clearBuffer();

    /*
     * disp.setFont(u8g2_font_6x13_tr);
     * disp.drawStr(72, 12, header);
     */

    disp.setFont(u8g2_font_6x10_tf);
    disp.drawStr(72, 29, header);

    displayQRCode(message);
    disp.sendBuffer();

    delay(random(1000, 15000));
}

void setup() {
    unsigned long seed = 0;

    for (int i=0; i<32; i++) {
      seed = seed | ((analogRead(A0) & 0x01) << i);
    }

    randomSeed(seed);
    disp.begin();
}

void displayQRCode(char * text) {
    QRCode qrcode;
    uint8_t qrcodeData[qrcode_getBufferSize(3)];
    qrcode_initText( & qrcode, qrcodeData, 3, 0, text);

    int scale = min(OLED_WIDTH / qrcode.size, OLED_HEIGHT / qrcode.size);

    int shiftX = (OLED_WIDTH - qrcode.size * scale) / 2;
    int shiftY = (OLED_HEIGHT - qrcode.size * scale) / 2;

    disp.setFont(u8g2_font_helvR24_tf);
    disp.drawStr(70, 60, text);

    for (uint8_t y = 0; y < qrcode.size; y++) {
        for (uint8_t x = 0; x < qrcode.size; x++) {
            if (qrcode_getModule( & qrcode, x, y)) {
                disp.drawBox(shiftX + x * scale, shiftY + y * scale, scale, scale);
            }
        }
    }
}
